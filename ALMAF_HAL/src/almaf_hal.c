/*
 * Created by: Marton Toth
 * HAL source file
 */

#include "almaf_hal.h"

/*################################################################################################*/
/*################################################################################################*/
/*################################################################################################*/


/*
 * Basic system functions
 *
 *  -- Oscillator & clocks
 *  -- SysTick
 *  -- NVIC
 *
 */
volatile uint32_t OSCFREQ = 0;

void RCC_SYSCLK_Configure( unsigned char source, unsigned char freq )
{
	if( source == HSI )
	{
		RCC->CR = 0x83;								// reset control register
		while( !(RCC->CR & 0x02) ) {}				// waiting for HSIRDY bit
		RCC->CFGR = 0;

		if( !(freq&3) && (65>freq) && (freq>8) )	// if true, PLL need to be used
		{
			if( freq <= 24 ) FLASH->ACR = 0x10;
			if( (24 < freq) && (freq <= 48) ) FLASH->ACR = 0x11;
			if( (48 < freq) && (freq <= 64) ) FLASH->ACR = 0x12;

			// if( 49 > freq ) RCC->CFGR |= (1 << 22);	// USB prescaler

			if( freq > 28 ) RCC->CFGR |= (1 << 14);	// ADC prescaler (max 14 MHZ)
			if( freq > 56 ) RCC->CFGR |= (2 << 14);
			if( freq > 36 ) RCC->CFGR |= (4 << 8);	// APB1 prescaler (max 36 MHZ)

			RCC->CFGR |= ( ((freq/4-2)) << 18 );	// calculate & set PLL multiplication

			RCC->CFGR |= 2;							// select PLL as system clock & HSI/2 as PLL input

			RCC->CR |= ( 1 << 24 );					// enable PLL
			while( !(RCC->CR & (1 << 25)) ) {}

			OSCFREQ = freq*1000000;

		}
		else return;								// at this point, the SYSCLK is HSI 8 MHZ
	}
}

// ********************************************************************************************** //


void SYSTICK_Init(uint32_t ticks)
{
	SysTick->CTRL = 0;			// first disable the system timer

	SysTick->LOAD = ticks-1;
	NVIC_SetPriority(SysTick_IRQn, (1 << __NVIC_PRIO_BITS)-1);
	SysTick->VAL = 0;
	SysTick->CTRL = 7;			//SYSCLK, interrupt enabled, counter enabled

}




/*################################################################################################*/
/*################################################################################################*/
/*################################################################################################*/

/*
 * GPIO configuration and control
 *
 */

void GPIO_PIN_Configure(GPIO_TypeDef *GPIOx, unsigned char GPIO_PIN, unsigned char PIN_MODE)
{
	// Prepare temporary variables //

	uint32_t temp = GPIOx->CRL;
	uint16_t msk = GPIO_PIN;

	if( GPIO_PIN > 7 )
	{
		msk -= 8;
		temp = GPIOx->CRH;
	}

	// Configure the selected pin //

	temp &= ( ~( 0x0F << (msk*4) ) );
	temp |= ( (PIN_MODE & 0x0F) << (msk*4) );

	if( GPIO_PIN > 7 ) GPIOx->CRH = temp;
	else GPIOx->CRL = temp;

	// Activate the pull-up/pull-down resistors //
	msk = ( 1 << GPIO_PIN );
	uint16_t clrmsk = ~(msk);

	if( (PIN_MODE & 0x1F) == INPUT_PU ) GPIOx->BSRR |= msk;
	if( (PIN_MODE & 0x1F) == INPUT_PD ) GPIOx->BRR |= msk;

	if( (PIN_MODE & 0x0F) > 8 ) AFIO_ClockEnable();

	// Configure external interrupts & events //

	temp = PIN_MODE & 0xE0;

	if( temp )
	{
		AFIO_ClockEnable();

		uint32_t tmpmsk = 0;

		if( GPIOx == GPIOA ) tmpmsk = 0;
		if( GPIOx == GPIOB ) tmpmsk = 1;
		if( GPIOx == GPIOC ) tmpmsk = 2;
		if( GPIOx == GPIOD ) tmpmsk = 3;

		tmpmsk = ( tmpmsk << ((GPIO_PIN&3) << 2) );

		AFIO->EXTICR[(GPIO_PIN >> 2)] = ( AFIO->EXTICR[(GPIO_PIN >> 2)] & ~( 0x0F << (GPIO_PIN&3 << 2) ) ) | tmpmsk;

		if( temp & FALLING ) EXTI->FTSR |= msk;
		else EXTI->FTSR &= clrmsk;
		if( temp & RISING ) EXTI->RTSR |= msk;
		else EXTI->RTSR &= clrmsk;


		if( temp & EXTI_MODE )
		{
			  EXTI->IMR |= msk;
			  EXTI->EMR &= clrmsk;
		}
		else
		{
			  EXTI->EMR |= msk;
			  EXTI->IMR &= clrmsk;
		}
	}
}

// ********************************************************************************************** //


void GPIO_WritePin(GPIO_TypeDef *GPIOx, unsigned char GPIO_PIN, unsigned char state)
{
	if( state ) GPIOx->BSRR |= ( 1 << GPIO_PIN );
	else GPIOx->BRR |= ( 1 << GPIO_PIN );
}

// ********************************************************************************************** //


unsigned char GPIO_ReadPin(GPIO_TypeDef *GPIOx, unsigned char GPIO_PIN)
{
	if( GPIOx->IDR & (1 << GPIO_PIN) ) return 1;
	else return 0;
}

// ********************************************************************************************** //

void GPIO_TogglePin(GPIO_TypeDef *GPIOx, unsigned char GPIO_PIN)
{
	GPIOx->ODR ^= (1 << GPIO_PIN);
}

/*################################################################################################*/
/*################################################################################################*/
/*################################################################################################*/


/*
 *	USART configuration
 *
 */
void USART_Remap( USART_TypeDef *USARTx, uint8_t state )
{
	switch( state )
	{
		case 2:	if( USARTx == USART3 ) AFIO->MAPR = (AFIO->MAPR&(~0x30))|0x10;
				break;

		case 1: if( USARTx == USART3 ) AFIO->MAPR |= (3 << 4);
				if( USARTx == USART2 ) AFIO->MAPR |= (1 << 3);
				if( USARTx == USART1 ) AFIO->MAPR |= (1 << 2);
				break;

		case 0: if( USARTx == USART3 ) AFIO->MAPR &= ~(3 << 4);
				if( USARTx == USART2 ) AFIO->MAPR &= ~(1 << 3);
				if( USARTx == USART1 ) AFIO->MAPR &= ~(1 << 2);
				break;

		default: break;
	}
}


void USART_Init( USART_TypeDef *USARTx, uint32_t baud, uint32_t mode)
{
	GPIO_TypeDef *GPIOx;
	uint8_t PIN_RX = 0;
	uint8_t PIN_TX = 0;
	uint32_t temp = AFIO->MAPR;

	if( USARTx == USART1 )
	{
		if(temp&4){ GPIOx=GPIOB; PIN_RX=7; PIN_TX=6; }
		else { GPIOx=GPIOA; PIN_RX=10; PIN_TX=9; }
		RCC->APB2ENR |= ( 1 << 14 );
	}
	else 	if( USARTx == USART2 )
			{
				if(temp&8){ GPIOx=GPIOD; PIN_RX=6; PIN_TX=5; }
				else { GPIOx=GPIOA; PIN_RX=3; PIN_TX=2; }
				RCC->APB1ENR |= ( 1 << 17 );
			}
			else	if( USARTx == USART3 )
			{
				if((temp&0x30)==0x10){ GPIOx=GPIOC; PIN_RX=11; PIN_TX=10; }
				if((temp&0x30)==0x30){ GPIOx=GPIOD; PIN_RX=9; PIN_TX=8; }
				if(!(temp&0x30)) { GPIOx=GPIOB; PIN_RX=11; PIN_TX=10; }
				RCC->APB1ENR |= ( 1 << 18 );
			}

	GPIO_PIN_Configure(GPIOx, PIN_RX, FLOAT_IN);
	GPIO_PIN_Configure(GPIOx, PIN_TX, ALT_PUSHPULL_HS);

	USARTx->CR1 = 0;

	if( mode & 0xF0000 ) USARTx->CR2 |= (2 << 12);
	else USARTx->CR2 &= ~(2 << 12);

	temp = OSCFREQ/(16*baud);
	USARTx->BRR = ( ((int)temp) << 4);

	USARTx->CR1 |= ( (mode&0xFFFF) | (1 << 13) );
}


void USART_IT_Configure(USART_TypeDef *USARTx, uint16_t mode, uint8_t state)
{
	if( state ) USARTx->CR1 |= mode;
	else USARTx->CR1 &= (~mode);
}






