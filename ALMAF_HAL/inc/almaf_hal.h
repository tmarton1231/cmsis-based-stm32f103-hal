/* created by: Marton Toth
 * required: ARM-ST-CMSIS files
 *
 * */

#ifndef ALMAF1231_HAL_H_
#define ALMAF1231_HAL_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "stm32f1xx.h"

/* ################################################################################################## *
 *
 * 	Oscillator & clock configuration
 *
 * ################################################################################################## */


#define HSI 0
#define HSE 1

void RCC_SYSCLK_Configure( unsigned char source, unsigned char freq );
void SYSTICK_Init( uint32_t ticks );

/* ################################################################################################## *
 *
 * 	GPIO Configuration
 *
 * ################################################################################################## */

#define ANALOG_IN 	0
#define FLOAT_IN 	4
#define INPUT_PU 	8
#define INPUT_PD 	0x18

#define EXTI_MODE		0x20
#define FALLING			0x40
#define	RISING			0x80
#define CHANGE			0xC0


#define PUSHPULL_OUT_LS 2
#define PUSHPULL_OUT_MS 1
#define PUSHPULL_OUT_HS 3

#define OPENDRAIN_OUT_LS 6
#define OPENDRAIN_OUT_MS 5
#define OPENDRAIN_OUT_HS 7

#define ALT_PUSHPULL_LS 10
#define ALT_PUSHPULL_MS 9
#define ALT_PUSHPULL_HS 11

#define ALT_OPENDRAIN_LS 14
#define ALT_OPENDRAIN_MS 13
#define ALT_OPENDRAIN_HS 15

#define GPIO_ClockEnableAll() RCC->APB2ENR |= 0x1FC;
#define AFIO_ClockEnable() RCC->APB2ENR |= 1;


void GPIO_PIN_Configure(GPIO_TypeDef *GPIOx, unsigned char GPIO_PIN, unsigned char PIN_MODE);
void GPIO_WritePin(GPIO_TypeDef *GPIOx, unsigned char GPIO_PIN, unsigned char state);
void GPIO_TogglePin(GPIO_TypeDef *GPIOx, unsigned char GPIO_PIN);
unsigned char GPIO_ReadPin(GPIO_TypeDef *GPIOx, unsigned char GPIO_PIN);

/* ################################################################################################## *
 *
 * 	USART Configuration
 *
 * ################################################################################################## */

#define DATA_8BIT	0
#define DATA_9BIT	( 1 << 12 )
#define ODD_PARITY ( 3 << 9 )
#define EVEN_PARITY	( 2 << 9 )
#define NOPARITY 0
#define RX_EN 4
#define	TX_EN 8

#define ONE_STOP 0
#define TWO_STOP 0x20000

#define IDLEIE (1 << 4)
#define RXIE (1 << 5)
#define TXIE (1 << 7)
#define TCIE (1 << 6)

#define PARTIAL 2

void USART_Remap( USART_TypeDef *USARTx, uint8_t state );
void USART_Init( USART_TypeDef *USARTx, uint32_t baud, uint32_t mode);
void USART_IT_Configure(USART_TypeDef *USARTx, uint16_t mode, uint8_t state);


#ifdef __cplusplus
}
#endif

#endif /* ALMAF1231_HAL_H_ */
