#include "almaf_hal.h"

volatile uint32_t time = 0;
volatile uint8_t state = 0;

void SysTick_Handler(void)
{
	if( (++time) >=1000 )
	{
		GPIO_WritePin(GPIOC, 13, !GPIO_ReadPin(GPIOC,13));

		time = 0;
	}

	if(!(time%300)) NVIC_EnableIRQ(EXTI4_IRQn);
	else 	  NVIC_DisableIRQ(EXTI4_IRQn);
}


void EXTI4_IRQHandler(void)
{
	 NVIC_DisableIRQ(EXTI4_IRQn);

	 GPIO_WritePin(GPIOB, 5, !GPIO_ReadPin(GPIOB,5));

	 NVIC_ClearPendingIRQ(EXTI4_IRQn);
	 EXTI->PR |= (1 << 4);
}

void USART1_IRQHandler(void)
{
	NVIC_ClearPendingIRQ(USART1_IRQn);
	GPIO_WritePin(GPIOB, 5, !GPIO_ReadPin(GPIOB,5));

	USART1->SR &= ~(1 << 5);
	state = (USART1->DR & 0xFF);
	USART1->DR = state+1;
}



int main(void)
{

  SystemInit();

  RCC_SYSCLK_Configure(HSI,24);
  SYSTICK_Init(24000);

  GPIO_ClockEnableAll();

  GPIO_PIN_Configure(GPIOC, 13, PUSHPULL_OUT_HS);
  GPIO_PIN_Configure(GPIOB,  4, INPUT_PU|EXTI_MODE|FALLING);
  GPIO_PIN_Configure(GPIOB, 5, OPENDRAIN_OUT_HS);

  GPIO_WritePin(GPIOB, 5, 1);

//  AFIO->MAPR |= (1<<2);
  USART_Remap(USART1, ENABLE);
  USART_Init( USART1, 9600, DATA_8BIT | NOPARITY | RX_EN | TX_EN | ONE_STOP );
  USART_IT_Configure( USART1, RXIE , ENABLE );



  __enable_irq();

  NVIC_ClearPendingIRQ(EXTI4_IRQn);
  NVIC_SetPriority(EXTI4_IRQn, 0);

  NVIC_ClearPendingIRQ(USART1_IRQn);
  NVIC_SetPriority(USART1_IRQn,0);

  NVIC_EnableIRQ(EXTI4_IRQn);
  NVIC_EnableIRQ(USART1_IRQn);


  while (1)
  {

  }

}

void Error_Handler(void)
{

}
